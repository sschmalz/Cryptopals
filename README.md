My solutions for the challenges found at cryptopals.com

Most solutions will be in C, and I will organize my solutions in folders by set.  If I use another language, such as Python, I will begin organizing my solutions by language as well.
