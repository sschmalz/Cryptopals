#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void hexToBinary(char*, char*);
char* hexQuads(char);
void binaryTo64(char*, char*);
void padZeroes(char*, int);

static const char quads[16][5] = {
	"0000",
	"0001",
	"0010",
	"0011",
	"0100",
	"0101",
	"0110",
	"0111",
	"1000",
	"1001",
	"1010",
	"1011",
	"1100",
	"1101",
	"1110",
	"1111"
};

static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int main(int argc, char* argv[]) {
	if (argc == 1) {
		printf("Invalid number of arguments\n");
		exit(0);
	}

	size_t argSize = sizeof(argv[1]);
	char* binaryString = malloc(4*(argSize+2));
	char* base64String = malloc(argSize);

	hexToBinary(argv[1], binaryString);

	binaryTo64(binaryString, base64String);

	printf("%s\n", base64String);

	free(binaryString);
	free(base64String);
	return 0;
}


void hexToBinary(char* hexString, char* binaryString) {
	int i, len = strlen(hexString);
	char c;

	for (i = 0; i < len; i++) {
		c = *(hexString+i);
		strcat(binaryString, hexQuads(c));
	}
}


char* hexQuads(char c) {
	if (c >= '0' && c <= '9') return quads[c - '0'];
	if (c >= 'A' && c <= 'Z') return quads[10 + c - 'A'];
	if (c >= 'a' && c <= 'z') return quads[10 + c - 'a'];
	return -1;
}


void binaryTo64(char* binaryString, char* base64String) {
	int len = strlen(binaryString);
	int zeroes = 6 - (len % 6);
	int binaryNum, i;
	char* binaryPiece = (char *) malloc(7 * sizeof(char));

	if (zeroes != 6) {
		padZeroes(binaryString, zeroes);
	}

	for (i = 0; i < len; i+=6) {
		strncpy(binaryPiece, binaryString + i, 6*sizeof(char));
		binaryNum = strtol(binaryPiece, NULL, 2);
		strncat(base64String, cb64+binaryNum, 1);
	}

	free(binaryPiece);
}

void padZeroes(char* binaryString, int numZeroes) {
	int size = sizeof(binaryString) + numZeroes, i;
	char* tempString = malloc(size);

	for (i = 0; i < numZeroes; i++) {
		strcat(tempString, "0");
	}
	strcat(tempString, binaryString);
	strcpy(binaryString, tempString);

	free(tempString);
	return;
}